package com.wakeel.tmdbapp.domain.repository

import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.PeopleResponse

/**
Created By M.EL-Wakeel
 */
interface PeopleRepository {
    fun getPeople(page: Int): Output<PeopleResponse>
}