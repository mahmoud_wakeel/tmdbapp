package com.wakeel.tmdbapp.domain.usecase

import com.wakeel.tmdbapp.core.base.BaseUseCase
import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.PeopleResponse
import com.wakeel.tmdbapp.domain.repository.PeopleRepository

/**
Created By M.EL-Wakeel
 */
class PeopleUseCase(var peopleRepository: PeopleRepository) :
    BaseUseCase<PeopleResponse, PeopleUseCase.Params>() {
    override suspend fun run(params: Params): Output<PeopleResponse> {
        return peopleRepository.getPeople(params.page)
    }

    data class Params(val page: Int)
}