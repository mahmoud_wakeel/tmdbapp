package com.wakeel.tmdbapp.domain.repository

import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.PersonDetailsResponse

/**
Created By M.EL-Wakeel
 */
interface PersonDetailsRepository {
    fun getPersonDetails(personID: String): Output<PersonDetailsResponse>
}