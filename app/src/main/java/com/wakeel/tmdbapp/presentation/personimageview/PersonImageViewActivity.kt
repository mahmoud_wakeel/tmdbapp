package com.wakeel.tmdbapp.presentation.personimageview

import android.Manifest
import android.app.DownloadManager
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.bumptech.glide.Glide
import com.wakeel.tmdbapp.R
import com.wakeel.tmdbapp.core.util.Constants
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


/**
Created By M.EL-Wakeel
 */
class PersonImageViewActivity : AppCompatActivity() {
    private var imagePath = ""
    private var name = ""
    private lateinit var personImg: ImageView
    private val PERMISSION_REQUEST_STORAGE = 1001
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_person_image)
        personImg = findViewById(R.id.person_img)
        imagePath = intent.getStringExtra(Constants.IMAGE_PATH)!!
        name = intent.getStringExtra(Constants.PERSON_NAME)!!
        title = name
        Glide.with(this)
            .load(imagePath)
            .into(personImg)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.download_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.download -> {
                if (ActivityCompat.checkSelfPermission(
                        this@PersonImageViewActivity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    saveImage()
                } else {
                    ActivityCompat.requestPermissions(
                        this@PersonImageViewActivity,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        PERMISSION_REQUEST_STORAGE
                    )
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_REQUEST_STORAGE) {
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    saveImage()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                Toast.makeText(
                    this@PersonImageViewActivity,
                    "You have to enable the permission!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun saveImage() {
        val manager = this.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        Downloading(manager, this@PersonImageViewActivity).execute(imagePath)
    }

    class Downloading(
        val manager: DownloadManager,
        val context: Context
    ) : AsyncTask<String?, Int?, String>() {
        public override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: String?): String {
            val myDir = File(Environment.getExternalStorageDirectory().toString())
            if (!myDir.exists()) {
                myDir.mkdirs()
            }
            val downloadUri: Uri = Uri.parse(params[0])
            val request = DownloadManager.Request(downloadUri)
            val dateFormat = SimpleDateFormat("mmddyyyyhhmmss")
            val date: String = dateFormat.format(Date())
            request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE
            )
                .setAllowedOverRoaming(false)
                .setTitle("Downloading")
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, "$date.jpg")
            manager.enqueue(request)
            return myDir.absolutePath + File.separator + date + ".jpg"
        }

        public override fun onPostExecute(s: String) {
            super.onPostExecute(s)
            Toast.makeText(context, "Image saved.", Toast.LENGTH_SHORT).show()
        }
    }
}