package com.wakeel.tmdbapp.core.util

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.wakeel.tmdbapp.R

class Configuration {
    companion object {
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500"

        fun loadImage(
            context: Context,
            imageView: ImageView,
            url: String
        ) {
            Glide.with(context)
                .load(url)
                .placeholder(R.drawable.placeholder)
                .into(imageView)
        }
    }
}