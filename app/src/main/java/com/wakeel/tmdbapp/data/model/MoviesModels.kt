package com.wakeel.tmdbapp.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
Created By M.EL-Wakeel
 */

data class KnownFor(
    @SerializedName("poster_path")
    @Expose
    var posterPath: String? = ""
)

data class Result(
    @SerializedName("popularity")
    @Expose
    var popularity: Double? = 0.0,
    @SerializedName("known_for_department")
    @Expose
    var knownForDepartment: String? = "",
    @SerializedName("name")
    @Expose
    var name: String? = "",
    @SerializedName("id")
    @Expose
    var id: Int? = 0,
    @SerializedName("profile_path")
    @Expose
    var profilePath: String? = "",
    @SerializedName("adult")
    @Expose
    var adult: Boolean? = false,
    @SerializedName("gender")
    @Expose
    var gender: Int? = 0,
    @SerializedName("known_for")
    @Expose
    var knownFor: ArrayList<KnownFor>? = ArrayList()
)

data class PeopleResponse(
    @SerializedName("results")
    @Expose
    var result: ArrayList<Result>? = ArrayList()
)

data class PersonDetailsResponse(
    @SerializedName("birthday")
    @Expose
    var birthday: String? = "",
    @SerializedName("known_for_department")
    @Expose
    var knownForDepartment: String? = "",
    @SerializedName("id")
    @Expose
    var id: Int? = 0,
    @SerializedName("name")
    @Expose
    var name: String? = "",
    @SerializedName("gender")
    @Expose
    var gender: Int? = 0,
    @SerializedName("biography")
    @Expose
    var biography: String? = "",
    @SerializedName("popularity")
    @Expose
    var popularity: String? = "",
    @SerializedName("place_of_birth")
    @Expose
    var placeOfBirth: String? = "",
    @SerializedName("profile_path")
    @Expose
    var profilePath: String? = "",
    @SerializedName("adult")
    @Expose
    var adult: Boolean? = false
)

data class Profile(
    @SerializedName("file_path")
    @Expose
    var filePath:String? = ""
)

data class PersonImagesResponse(
    @SerializedName("profiles")
    @Expose
    var profiles:ArrayList<Profile>? = ArrayList(),
    @SerializedName("id")
    @Expose
    var id:Int? = 0
)