package com.wakeel.tmdbapp.data.remote

import com.wakeel.tmdbapp.data.model.PeopleResponse
import com.wakeel.tmdbapp.data.model.PersonDetailsResponse
import com.wakeel.tmdbapp.data.model.PersonImagesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface PeopleDataSource {
    @GET("person/popular")
    fun getPopularPeople(
        @Query("api_key") apiKey: String,
        @Query("language") language: String,
        @Query("page") pageNumber: Int,
        @Query("region") region: String,
        @Query("with_release_type") releaseType: String
    ): Call<PeopleResponse>

    @GET("person/{person_id}")
    fun getPersonDetails(
        @Path("person_id") personId: String,
        @Query("api_key") apiKey: String,
        @Query("language") language: String
    ):Call<PersonDetailsResponse>

    @GET("person/{person_id}/images")
    fun getPersonImages(
        @Path("person_id") personId: String,
        @Query("api_key") apiKey: String,
        @Query("language") language: String
    ):Call<PersonImagesResponse>
}