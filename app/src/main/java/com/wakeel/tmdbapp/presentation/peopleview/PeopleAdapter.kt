package com.wakeel.tmdbapp.presentation.peopleview

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.wakeel.tmdbapp.R
import com.wakeel.tmdbapp.core.util.Configuration
import com.wakeel.tmdbapp.data.model.Result
import kotlin.math.floor

/**
Created By M.EL-Wakeel
 */
class PeopleAdapter(
    private val peopleList: ArrayList<Result>,
    private val context: Context,
    private val listener: OnItemClicked
) : RecyclerView.Adapter<PeopleAdapter.PersonViewHolder>() {

    class PersonViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.person_item, parent, false)) {
        var personImage = itemView.findViewById<ImageView>(R.id.person_img)!!
        var personName = itemView.findViewById<TextView>(R.id.person_name_txt)!!
        var personPopularity = itemView.findViewById<TextView>(R.id.person_popularity_txt)!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val inflater = LayoutInflater.from(context)
        return PersonViewHolder(
            inflater,
            parent
        )
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        val person = peopleList[position]
        Configuration.loadImage(context,holder.personImage,Configuration.IMAGE_BASE_URL + person.profilePath)
        holder.personName.text = person.name
        val popularity = floor(person.popularity!! * 100) / 100
        holder.personPopularity.text =
            context.getString(R.string.popularity_placeholder, popularity.toString())
        holder.itemView.setOnClickListener {
            listener.onItemClicked(person.id!!.toString(), person.name!!)
        }
    }

    override fun getItemCount() = peopleList.size

    interface OnItemClicked {
        fun onItemClicked(id: String, name: String)
    }
}