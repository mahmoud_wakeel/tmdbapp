package com.wakeel.tmdbapp.presentation.peopleview

import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.wakeel.tmdbapp.R
import com.wakeel.tmdbapp.core.util.Constants
import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.Result
import com.wakeel.tmdbapp.presentation.peopledetailsview.PersonDetailsActivity
import kotlinx.android.synthetic.main.activity_people.*
import org.koin.android.viewmodel.ext.android.viewModel


class PeopleActivity : AppCompatActivity(), PeopleAdapter.OnItemClicked {
    private val GRID_COLUMNS = 2
    private val peopleViewModel: PeopleViewModel by viewModel()
    var currentPage = 1
    var pageSize = 20
    var isLoading = false
    lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var peopleAdapter: PeopleAdapter
    private var people = ArrayList<Result>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_people)
        showProgressBar()
        peopleViewModel.getPeople(currentPage)
        observeData()
        initPeopleRecyclerView()
    }

    private fun initPeopleRecyclerView() {
        gridLayoutManager = GridLayoutManager(this@PeopleActivity, GRID_COLUMNS)
        recycler_view_people.layoutManager = gridLayoutManager
        peopleAdapter = PeopleAdapter(people, this@PeopleActivity, this@PeopleActivity)
        recycler_view_people.addOnScrollListener(recyclerViewOnScrollListener)
        recycler_view_people.adapter = peopleAdapter
    }

    private fun showProgressBar() {
        progressBar.visibility = VISIBLE
    }

    private fun hideProgressBar() {
        progressBar.visibility = GONE
    }

    override fun onItemClicked(id: String, name: String) {
        val detailsIntent = Intent(this@PeopleActivity, PersonDetailsActivity::class.java)
        detailsIntent.putExtra(Constants.PERSON_ID, id)
        detailsIntent.putExtra(Constants.PERSON_NAME, name)
        startActivity(detailsIntent)
    }

    private val recyclerViewOnScrollListener: RecyclerView.OnScrollListener =
        object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount: Int = gridLayoutManager.childCount
                val totalItemCount: Int = gridLayoutManager.itemCount
                val firstVisibleItemPosition: Int =
                    gridLayoutManager.findFirstVisibleItemPosition()
                if (!isLoading) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount &&
                        firstVisibleItemPosition >= 0 &&
                        totalItemCount >= pageSize
                    ) {
                        isLoading = true
                        currentPage += 1
                        showProgressBar()
                        peopleViewModel.getPeople(currentPage)
                    }
                }
            }
        }

    private fun observeData() {
        peopleViewModel.people.observe(this@PeopleActivity, Observer {
            when (it) {
                is Output.Success -> {
                    hideProgressBar()
                    people.addAll(it.output.result!!)
                    recycler_view_people.adapter!!.notifyDataSetChanged()
                    isLoading = false
                }

                is Output.Error -> {
                    hideProgressBar()
                    Toast.makeText(applicationContext, it.error, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}