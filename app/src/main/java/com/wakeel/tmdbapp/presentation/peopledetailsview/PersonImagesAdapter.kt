package com.wakeel.tmdbapp.presentation.peopledetailsview

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.wakeel.tmdbapp.R
import com.wakeel.tmdbapp.core.util.Configuration
import com.wakeel.tmdbapp.data.model.Profile

/**
Created By M.EL-Wakeel
 */
class PersonImagesAdapter(
    private val imageList: ArrayList<Profile>,
    private val name: String,
    private val context: Context,
    private val listener: OnImageClicked
) : RecyclerView.Adapter<PersonImagesAdapter.PersonImageViewHolder>() {
    class PersonImageViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.person_image_item, parent, false)) {
        var personImage = itemView.findViewById<ImageView>(R.id.person_img)!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonImageViewHolder {
        val inflater = LayoutInflater.from(context)
        return PersonImageViewHolder(
            inflater,
            parent
        )
    }

    override fun onBindViewHolder(holder: PersonImageViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        val profile = imageList[position]
        Glide.with(context)
            .load(Configuration.IMAGE_BASE_URL + profile.filePath)
            .into(holder.personImage)
        holder.itemView.setOnClickListener {
            listener.onItemClicked(Configuration.IMAGE_BASE_URL + profile.filePath, name)
        }
    }

    override fun getItemCount() = imageList.size

    interface OnImageClicked {
        fun onItemClicked(imagePath: String, name: String)
    }
}