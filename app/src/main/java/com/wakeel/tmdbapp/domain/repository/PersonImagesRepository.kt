package com.wakeel.tmdbapp.domain.repository

import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.PersonImagesResponse

/**
Created By M.EL-Wakeel
 */
interface PersonImagesRepository {
    fun getPersonImages(personID: String): Output<PersonImagesResponse>
}