package com.wakeel.tmdbapp.presentation.peopledetailsview

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.wakeel.tmdbapp.R
import com.wakeel.tmdbapp.core.util.Configuration
import com.wakeel.tmdbapp.core.util.Constants
import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.PersonDetailsResponse
import com.wakeel.tmdbapp.data.model.PersonImagesResponse
import com.wakeel.tmdbapp.presentation.personimageview.PersonImageViewActivity
import kotlinx.android.synthetic.main.activity_person.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
Created By M.EL-Wakeel
 */
class PersonDetailsActivity : AppCompatActivity(), PersonImagesAdapter.OnImageClicked {
    private val detailsViewModel: PersonDetailsViewModel by viewModel()
    private val imagesViewModel: PersonImagesViewModel by viewModel()
    private var personID = ""
    private var personName = ""
    private val GRID_COLUMNS = 4
    lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var personImageAdapter: PersonImagesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_person)
        personID = intent.getStringExtra(Constants.PERSON_ID)!!
        personName = intent.getStringExtra(Constants.PERSON_NAME)!!
        title = personName
        showProgressBar()
        detailsViewModel.getDetails(personID)
        observePersonDetails()
    }

    private fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    private fun loadPersonDetails(person: PersonDetailsResponse) {
        Configuration.loadImage(
            this@PersonDetailsActivity,
            person_img,
            Configuration.IMAGE_BASE_URL + person.profilePath
        )

        person_name_txt.text = person.name
        person_name_txt.visibility = GONE

        person_birthday_txt.text = if (person.birthday == null) getString(
            R.string.birthday_placeholder,
            "N/A"
        ) else getString(R.string.birthday_placeholder, person.birthday)

        person_birth_place_txt.text =
            if (person.placeOfBirth == null) getString(R.string.birth_place_placeholder, "N/A")
            else getString(R.string.birth_place_placeholder, person.placeOfBirth)

        person_title_txt.text =
            if (person.knownForDepartment == null) getString(R.string.title_placeholder, "N/A")
            else getString(R.string.title_placeholder, person.knownForDepartment)

        person_popularity_txt.text = if (person.popularity == null) getString(
            R.string.popularity_placeholder,
            "N/A"
        ) else getString(R.string.popularity_placeholder, person.popularity)

        person_biography_txt.text = person.biography
    }

    private fun loadPersonImages(images: PersonImagesResponse) {
        gridLayoutManager = GridLayoutManager(this@PersonDetailsActivity, GRID_COLUMNS)
        person_images_list.layoutManager = gridLayoutManager
        personImageAdapter = PersonImagesAdapter(
            images.profiles!!,
            personName,
            this@PersonDetailsActivity,
            this@PersonDetailsActivity
        )
        person_images_list.adapter = personImageAdapter
    }

    private fun observePersonDetails() {
        detailsViewModel.details.observe(this@PersonDetailsActivity, Observer {
            when (it) {
                is Output.Success -> {
                    imagesViewModel.getImages(personID)
                    observePersonImages()
                    loadPersonDetails(it.output)
                }

                is Output.Error -> {
                    hideProgressBar()
                    Toast.makeText(applicationContext, it.error, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun observePersonImages() {
        imagesViewModel.images.observe(this@PersonDetailsActivity, Observer {
            when (it) {
                is Output.Success -> {
                    hideProgressBar()
                    loadPersonImages(it.output)
                }

                is Output.Error -> {
                    hideProgressBar()
                    Toast.makeText(applicationContext, it.error, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onItemClicked(imagePath: String, name: String) {
        val imageIntent = Intent(this@PersonDetailsActivity, PersonImageViewActivity::class.java)
        imageIntent.putExtra(Constants.PERSON_NAME, name)
        imageIntent.putExtra(Constants.IMAGE_PATH, imagePath)
        startActivity(imageIntent)
    }
}