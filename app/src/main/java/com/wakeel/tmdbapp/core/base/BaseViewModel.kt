package com.wakeel.tmdbapp.core.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

open class BaseViewModel(var useCaseLifeCycle: UseCaseLifeCycle) : ViewModel(), CoroutineScope {

    private var viewModelJob = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + viewModelJob

    override fun onCleared() {
        super.onCleared()
        useCaseLifeCycle.onCleared()
    }
}