package com.wakeel.tmdbapp.data.repository

import com.wakeel.tmdbapp.core.util.Constants
import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.PeopleResponse
import com.wakeel.tmdbapp.data.remote.PeopleDataSource
import com.wakeel.tmdbapp.domain.repository.PeopleRepository

/**
Created By M.EL-Wakeel
 */
class PopularPeopleRepositoryImpl : BaseRepository(), PeopleRepository {
    override fun getPeople(page: Int): Output<PeopleResponse> {
        return safeApiCall(
            remoteDataSource.makeRemoteCall(PeopleDataSource::class.java)
                .getPopularPeople(
                    Constants.API_KEY,
                    "en-US",
                    page,
                    "US",
                    "2|3"
                ),
            transform = { it },
            default = PeopleResponse(),
            error = ""
        )
    }

}