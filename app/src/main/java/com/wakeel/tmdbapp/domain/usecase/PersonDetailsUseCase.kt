package com.wakeel.tmdbapp.domain.usecase

import com.wakeel.tmdbapp.core.base.BaseUseCase
import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.PersonDetailsResponse
import com.wakeel.tmdbapp.domain.repository.PersonDetailsRepository

/**
Created By M.EL-Wakeel
 */
class PersonDetailsUseCase(var personDetailsRepository: PersonDetailsRepository) :
    BaseUseCase<PersonDetailsResponse, PersonDetailsUseCase.Params>() {

    override suspend fun run(params: Params): Output<PersonDetailsResponse> {
        return personDetailsRepository.getPersonDetails(params.personID)
    }

    data class Params(val personID: String)
}