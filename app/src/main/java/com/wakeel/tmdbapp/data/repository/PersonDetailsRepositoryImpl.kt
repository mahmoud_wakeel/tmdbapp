package com.wakeel.tmdbapp.data.repository

import com.wakeel.tmdbapp.core.util.Constants
import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.PersonDetailsResponse
import com.wakeel.tmdbapp.data.remote.PeopleDataSource
import com.wakeel.tmdbapp.domain.repository.PersonDetailsRepository

/**
Created By M.EL-Wakeel
 */
class PersonDetailsRepositoryImpl : BaseRepository(), PersonDetailsRepository {
    override fun getPersonDetails(personID:String): Output<PersonDetailsResponse> {
        return safeApiCall(
            remoteDataSource.makeRemoteCall(PeopleDataSource::class.java)
                .getPersonDetails(
                    personID,
                    Constants.API_KEY,
                    "en-US"
                ),
            transform = { it },
            default = PersonDetailsResponse(),
            error = ""
        )
    }
}