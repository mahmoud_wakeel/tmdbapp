package com.wakeel.tmdbapp.data.model

enum class ErrorType {
    GENERAL, DATA, CONNECTION
}