package com.wakeel.tmdbapp.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.PeopleResponse
import com.wakeel.tmdbapp.domain.repository.PeopleRepository
import com.wakeel.tmdbapp.domain.usecase.PeopleUseCase
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
Created By M.EL-Wakeel
 */
class PeopleUseCaseTest {

    companion object {
        private val page = 1
    }

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var peopleRepo: PeopleRepository

    lateinit var peopleUseCase: PeopleUseCase

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        peopleUseCase = PeopleUseCase(peopleRepo)
    }

    @Test
    fun peopleUseCase_fetchPeople() {
        Mockito.`when`(peopleRepo.getPeople(page))
            .thenReturn(Output.Success(PeopleResponse()))
        val output = runBlocking { peopleUseCase.run(PeopleUseCase.Params(page)) }
        assert(output is Output.Success)
    }
}