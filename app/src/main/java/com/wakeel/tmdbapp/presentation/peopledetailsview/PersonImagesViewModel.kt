package com.wakeel.tmdbapp.presentation.peopledetailsview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wakeel.tmdbapp.core.base.BaseViewModel
import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.PersonImagesResponse
import com.wakeel.tmdbapp.domain.usecase.PersonImagesUseCase

/**
Created By M.EL-Wakeel
 */
class PersonImagesViewModel(var personImagesUseCase: PersonImagesUseCase) :
    BaseViewModel(personImagesUseCase) {
    private val _images = MutableLiveData<Output<PersonImagesResponse>>()
    val images: LiveData<Output<PersonImagesResponse>> = _images

    fun getImages(personID: String) {
        personImagesUseCase(PersonImagesUseCase.Params(personID)) {
            _images.value = it
        }
    }

    override fun onCleared() {
        super.onCleared()
        personImagesUseCase.onCleared()
    }
}