package com.wakeel.tmdbapp.data.repository

import com.wakeel.tmdbapp.core.util.Constants
import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.PersonImagesResponse
import com.wakeel.tmdbapp.data.remote.PeopleDataSource
import com.wakeel.tmdbapp.domain.repository.PersonImagesRepository

/**
Created By M.EL-Wakeel
 */
class PersonImagesRepositoryImpl:BaseRepository(),PersonImagesRepository {
    override fun getPersonImages(personID: String): Output<PersonImagesResponse> {
        return safeApiCall(
            remoteDataSource.makeRemoteCall(PeopleDataSource::class.java)
                .getPersonImages(
                    personID,
                    Constants.API_KEY,
                    "en-US"
                ),
            transform = { it },
            default = PersonImagesResponse(),
            error = ""
        )
    }
}