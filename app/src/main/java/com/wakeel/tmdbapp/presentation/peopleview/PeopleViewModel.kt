package com.wakeel.tmdbapp.presentation.peopleview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wakeel.tmdbapp.core.base.BaseViewModel
import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.PeopleResponse
import com.wakeel.tmdbapp.domain.usecase.PeopleUseCase

/**
Created By M.EL-Wakeel
 */
class PeopleViewModel(var peopleUseCase: PeopleUseCase) : BaseViewModel(peopleUseCase) {
    private val _people = MutableLiveData<Output<PeopleResponse>>()
    val people: LiveData<Output<PeopleResponse>> = _people

    fun getPeople(page: Int) {
        peopleUseCase(PeopleUseCase.Params(page)) {
            _people.value = it
        }
    }

    override fun onCleared() {
        super.onCleared()
        peopleUseCase.onCleared()
    }
}