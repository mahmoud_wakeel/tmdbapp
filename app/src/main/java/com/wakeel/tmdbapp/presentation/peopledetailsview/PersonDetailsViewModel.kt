package com.wakeel.tmdbapp.presentation.peopledetailsview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wakeel.tmdbapp.core.base.BaseViewModel
import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.PersonDetailsResponse
import com.wakeel.tmdbapp.domain.usecase.PersonDetailsUseCase

/**
Created By M.EL-Wakeel
 */
class PersonDetailsViewModel(var personDetailsUseCase: PersonDetailsUseCase) :
    BaseViewModel(personDetailsUseCase) {
    private val _details = MutableLiveData<Output<PersonDetailsResponse>>()
    val details: LiveData<Output<PersonDetailsResponse>> = _details

    fun getDetails(personID: String) {
        personDetailsUseCase(PersonDetailsUseCase.Params(personID)) {
            _details.value = it
        }
    }

    override fun onCleared() {
        super.onCleared()
        personDetailsUseCase.onCleared()
    }
}