package com.wakeel.tmdbapp.core.di

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.wakeel.tmdbapp.core.util.Configuration
import com.wakeel.tmdbapp.data.network.RemoteSourceManager
import com.wakeel.tmdbapp.data.repository.PersonDetailsRepositoryImpl
import com.wakeel.tmdbapp.data.repository.PersonImagesRepositoryImpl
import com.wakeel.tmdbapp.data.repository.PopularPeopleRepositoryImpl
import com.wakeel.tmdbapp.domain.repository.PeopleRepository
import com.wakeel.tmdbapp.domain.repository.PersonDetailsRepository
import com.wakeel.tmdbapp.domain.repository.PersonImagesRepository
import com.wakeel.tmdbapp.domain.usecase.PeopleUseCase
import com.wakeel.tmdbapp.domain.usecase.PersonDetailsUseCase
import com.wakeel.tmdbapp.domain.usecase.PersonImagesUseCase
import com.wakeel.tmdbapp.presentation.peopledetailsview.PersonDetailsViewModel
import com.wakeel.tmdbapp.presentation.peopledetailsview.PersonImagesViewModel
import com.wakeel.tmdbapp.presentation.peopleview.PeopleViewModel
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Modules {
    val networkModule = module {

        fun provideRetrofit(client: OkHttpClient): Retrofit {

            return Retrofit.Builder().client(client)
                .baseUrl(Configuration.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
        }

        fun provideClient(
            interceptor: Interceptor,
            httpLoggingInterceptor: HttpLoggingInterceptor
        ): OkHttpClient {

            return OkHttpClient().newBuilder().addInterceptor(interceptor)
                .addInterceptor(httpLoggingInterceptor)
                .build()
        }


        fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {

            val httpLoggingInterceptor = HttpLoggingInterceptor()

            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            return httpLoggingInterceptor
        }


        fun provideInterceptor(): Interceptor {

            return Interceptor.invoke {

                val url =
                    it.request().url.newBuilder().build()
                val request = it.request()
                    .newBuilder()
                    .url(url)
                    .build()
                it.proceed(request)

            }
        }

        single { provideRetrofit(get()) }
        single { provideClient(get(), get()) }
        single { provideInterceptor() }
        single { provideHttpLoggingInterceptor() }
        single { RemoteSourceManager(get()) }
    }

    //use cases
    val peopleUseCaseModule: Module = module {
        factory { PeopleUseCase(peopleRepository = get()) }
    }

    val personDetailsUseCaseModule: Module = module {
        factory { PersonDetailsUseCase(personDetailsRepository = get()) }
    }

    val personImagesUseCaseModule: Module = module {
        factory { PersonImagesUseCase(personImagesRepository = get()) }
    }

    //repositories
    val repoPopularPeopleModule = module {
        single<PeopleRepository> { PopularPeopleRepositoryImpl() }
    }

    val repoPersonDetailsModule = module {
        single<PersonDetailsRepository> { PersonDetailsRepositoryImpl() }
    }

    val repoPersonImagesModule = module {
        single<PersonImagesRepository> { PersonImagesRepositoryImpl() }
    }

    //view models
    val peopleViewModelModule = module {
        viewModel { PeopleViewModel(get()) }
    }

    val detailsViewModelModule = module {
        viewModel { PersonDetailsViewModel(get()) }
    }

    val imagesViewModelModule = module {
        viewModel { PersonImagesViewModel(get()) }
    }

}


