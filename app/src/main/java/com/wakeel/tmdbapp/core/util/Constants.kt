package com.wakeel.tmdbapp.core.util

object Constants {
    const val API_KEY = "d3f9f87e5cc0c9e1f3c980a933d7ced9"
    const val PERSON_ID = "PERSON_ID"
    const val PERSON_NAME = "PERSON_NAME"
    const val IMAGE_PATH = "IMAGE_PATH"
}