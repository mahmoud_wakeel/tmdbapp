package com.wakeel.tmdbapp.core.util

import android.app.Application
import com.wakeel.tmdbapp.core.di.Modules.detailsViewModelModule
import com.wakeel.tmdbapp.core.di.Modules.imagesViewModelModule
import com.wakeel.tmdbapp.core.di.Modules.networkModule
import com.wakeel.tmdbapp.core.di.Modules.peopleUseCaseModule
import com.wakeel.tmdbapp.core.di.Modules.peopleViewModelModule
import com.wakeel.tmdbapp.core.di.Modules.personDetailsUseCaseModule
import com.wakeel.tmdbapp.core.di.Modules.personImagesUseCaseModule
import com.wakeel.tmdbapp.core.di.Modules.repoPersonDetailsModule
import com.wakeel.tmdbapp.core.di.Modules.repoPersonImagesModule
import com.wakeel.tmdbapp.core.di.Modules.repoPopularPeopleModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
Created By M.EL-Wakeel
 */
class TMDbApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@TMDbApplication)
            modules(
                listOf(
                    networkModule,
                    peopleUseCaseModule,
                    personDetailsUseCaseModule,
                    personImagesUseCaseModule,
                    repoPopularPeopleModule,
                    repoPersonDetailsModule,
                    repoPersonImagesModule,
                    peopleViewModelModule,
                    detailsViewModelModule,
                    imagesViewModelModule
                )
            )
        }
    }
}