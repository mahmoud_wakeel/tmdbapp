package com.wakeel.tmdbapp.domain.usecase

import com.wakeel.tmdbapp.core.base.BaseUseCase
import com.wakeel.tmdbapp.data.model.Output
import com.wakeel.tmdbapp.data.model.PersonImagesResponse
import com.wakeel.tmdbapp.domain.repository.PersonImagesRepository

/**
Created By M.EL-Wakeel
 */
class PersonImagesUseCase(var personImagesRepository: PersonImagesRepository) :
    BaseUseCase<PersonImagesResponse, PersonImagesUseCase.Params>() {
    override suspend fun run(params: Params): Output<PersonImagesResponse> {
        return personImagesRepository.getPersonImages(params.personID)
    }

    data class Params(val personID: String)
}